<?php
/** Langx.class.php
 * author: Yves Lange
 * version: 1.3
 *
 * INSTALLATION (very important)
 * Since Langx use session and cookie to maintain the user choice,
 * it is necessary to include the framework at the begining of the
 * file where you want to use it. The best choice will be to include
 * the framework in the index.php (or wathever) just after a session_start().
 * Example in index.php:
 * ---------------------
 *  <?php
 *  session_start(); 
 *  include_once("classes/Langx.class.php");
 *  ?>
 * --------------------- 
 *
 * ADDING LANGUAGE
 * The only thing you'll need to do is to create a new file according
 * to the syntax in the 'langx' folder: langx.[language_prefix].php. 
 * Exemple for french: "langx/langx.fr.php"
 * The new file must have an $_LANGX array containing the key and values
 * of the sentences: 
 * ---------------------
 *  $_LANGX = array(
 *    "home_test"=>"ceci est un petit test",
 *    "home_test2"=>"ceci est notre deuxième test",
 *  );
 * ---------------------
 *
 *
 * USAGE EXAMPLE
 * ---------------------
 *  <?php $langx->printxbr("home_test"); ?>
 *  <?php $langx->printxbr("home_test2"); ?>
 * ---------------------
 */

class Langx{
  // Default langage prefix
  private $sDefaultLangPrefix = "us";
  private $sCurrentLangPrefix = null;
  
  // Default langage directory
  private $sDefaultLangDirectory = "langx/";
  private $sFlagsDirectory = "langx-flags/";
  private $sRealpath;

  // Error prefixes and stats
  private $LANGX_ERROR = "LANGX ERROR";
  private $LANGX_WARN  = "LANGX WARNING";
  private $LANGX_ERROR_ON = true;
  private $LANGX_WARN_ON  = true;
  
  // The langage variable array
  private $aLangage = null;

  ////
  // CONSTRUCTOR
  ////
  function __construct(){
    // Setting the realpath
    $this->sRealpath = realpath(dirname(__FILE__))."/";

    // Loading default langage file (to ensure every variable is defined)
    $sLangFilename = $this->getFilename();
    include($sLangFilename);
    $this->aLangage = $_LANGX;
  }
  
  ////
  // PRIVATE FUNCTIONS
  ////
  
  // Get the filename from a prefix
  private function getFilename($sLangPrefix=null){
    if(!$sLangPrefix){ $sLangPrefix = $this->sDefaultLangPrefix; }
    $sLangFilename = "langx.".$sLangPrefix.".php"; 
    return $this->sRealpath.$this->sDefaultLangDirectory.$sLangFilename;
  }
  
  // Getting a specific sentence
  private function getSentence($sSentence){
    $sReturn = null;
    if(isset($this->aLangage[$sSentence])){    
      $sReturn = $this->aLangage[$sSentence];
    }
    else{ $this->printWarning("Word '".$sSentence."' not found"); }
    
    return $sReturn;
  }
 
  // Printing an error
  private function printError($sMsg=null){
    if($this->LANGX_ERROR_ON){ trigger_error("[".$this->LANGX_ERROR."]: ".$sMsg, E_USER_ERROR); }
  }
  
  // Printing a warning
  private function printWarning($sMsg=null){
    if($this->LANGX_WARN_ON){ trigger_error("[".$this->LANGX_WARN."]: ".$sMsg, E_USER_WARNING); }
  }
  
  ////
  // PUBLIC FUNCTIONS
  ////
  
  // Change the current langage
  public function changeLangage($sLangPrefix){

    // If the prefix is null or the langage is already loaded
    if(!$sLangPrefix || $sLangPrefix == $this->sCurrentLangPrefix){ return; }
    // Loading the specific langage file
    $sLangFilename = $this->getFilename($sLangPrefix);
  
    // If the file doesn't exists, loading the default file
    if(file_exists($sLangFilename)){
      $this->sCurrentLangPrefix = $sLangPrefix;
      include($sLangFilename);
      $this->aLangage = array_merge($this->aLangage, $_LANGX);
    }
    else{$this->printWarning("Prefix '".$sLangPrefix."' unknown, file '".$sLangFilename."' not found !");} 
  }

  // Get all the langage
  public function listLangage(){
    $aReturn = array();
    if ($handle = opendir($this->sRealpath.$this->sDefaultLangDirectory)) {
      while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          $aReturn[] = explode(".", $entry)[1];
        }
      }
      closedir($handle);
    }
    return $aReturn;
  }

  public function getImg($sRootdir, $sLangPrefix, $iSize){
    return "<img src='".$sRootdir.$this->sFlagsDirectory.
    $iSize."/".$sLangPrefix.".png' title='Flag ".$sLangPrefix."' />";
  }
  
  // Printing the selected sentence
  public function printx($sSentence=null){
    echo $this->getSentence($sSentence);
  }
  
  // Printing the selected sentence with <br> tag.
  public function printxbr($sSentence=null){
    echo $this->getSentence($sSentence)."<br>";
  }
  
  // Displaying errors or warnings
  public function setDebug($bErrors=false, $bWarnings=false){
    $this->LANGX_ERROR_ON = $bErrors;
    $this->LANGX_WARN_ON = $bWarnings;
  }

  // Get the langage prefix
  public function getCurrentPrefix(){
    return $this->sCurrentLangPrefix;
  }
}


////
// AUTO INIT
////

$langx = new Langx();

// The cookie will expire in twenty years
$_LANGX_iCookieExpire = time() + (20 * 365 * 24 * 60 * 60);
$_LANGX_default_langage = "en";

// Init with session and cookie
if(isset($_GET["lang"])){ 
  $_SESSION["LANGX"] = addslashes($_GET["lang"]);
  setcookie("LANGX", addslashes($_GET["lang"]), $_LANGX_iCookieExpire);
}
elseif(isset($_COOKIE["LANGX"])){
  $_SESSION["LANGX"] = addslashes($_COOKIE["LANGX"]);
}
else{
  $_SESSION["LANGX"] = $_LANGX_default_langage;
  setcookie("LANGX", $_LANGX_default_langauge, $_LANGX_iCookieExpire);
}
$langx->changeLangage($_SESSION["LANGX"]);



